(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'dashboard' : '/dashboard'
    }
  });

}(document));
